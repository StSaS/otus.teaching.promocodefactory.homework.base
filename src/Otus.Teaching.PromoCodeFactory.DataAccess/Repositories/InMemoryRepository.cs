﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }
        private List<Employee> Employees;

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
            Employees = Data as List<Employee>;
            if (Employees == null) Employees = new List<Employee>();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        private Guid AddEmployees(Employee employee)
        {
            Employees.Add(employee);
           
            return employee.Id;
        }

        private int DeleteEmployees(Employee employee)
        {
            int result = 0;
            
            if (Employees.Count == 0) return 0;

            result = Employees.RemoveAll(x => (x.FirstName == employee.FirstName && x.LastName == employee.LastName)
                                    || x.Id == employee.Id);
            
            return result;
        }

        private bool UpdateEmployees(Employee employee)
        {
            if ( Employees.Count == 0) return false;
            var updatebleEmployes = from emp in Employees
                                    where emp.Id.Equals(employee.Id) 
                                        || (emp.LastName == employee.LastName
                                        && emp.FirstName == employee.FirstName)
                                    select emp;

            foreach(Employee emp in updatebleEmployes)
            {
                emp.LastName = employee.LastName;
                emp.FirstName = employee.FirstName;
                emp.Email = employee.Email;
            }
          
            return true;
        }

        public Task<Guid> Add(T entity)
        {
            Guid ID = Guid.Empty;
            try
            {
                if (entity is Employee e)
                {
                    ID = AddEmployees(e);
                }
            }
            catch
            {
                return Task.FromResult(ID);
            }
            return Task.FromResult(ID);
        }

        public Task<bool> Update(T entity)
        {
            bool result = false;
            try
            {
                if (entity is Employee e)
                {
                    result = UpdateEmployees(e);
                }
            }
            catch
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(result);
        }

        public Task<int> Delete(T entity)
        {
            int result = 0;
            try
            {
                if (entity is Employee e)
                {
                    result = DeleteEmployees(e);
                }
            }
            catch
            {
                return Task.FromResult(0);
            }
            return Task.FromResult(result);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
    }
}