﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Добавить сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/[controller]/Add")]
        public async Task<Guid> AddEmployeeAsync(EmployeeRequest body)
        {
            Guid result = await ((InMemoryRepository<Employee>)_employeeRepository)
                .Add(new Employee()
                        {
                            Id = body.Id.Equals(Guid.Empty) ? Guid.NewGuid(): body.Id,
                            FirstName= body.FirstName,
                            LastName = body.LastName,
                            Email = body.Email
                        });

            return result;
        }

        /// <summary>
        /// Редактирование сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/[controller]/Update")]
        public async Task<bool> UpdateEmployeeAsync(EmployeeRequest body)
        {
            bool result = await ((InMemoryRepository<Employee>)_employeeRepository)
                .Update(new Employee()
                {
                    Id = body.Id,
                    FirstName = body.FirstName,
                    LastName = body.LastName,
                    Email = body.Email
                });

            return result;
        }

        /// <summary>
        /// Удаление сотрудника.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/[controller]/Delete")]
        public async Task<int> DeleteEmployeeAsync(EmployeeRequest body)
        {
            int result = await ((InMemoryRepository<Employee>)_employeeRepository)
                .Delete(new Employee()
                {
                    Id = body.Id,
                    FirstName = body.FirstName,
                    LastName = body.LastName,
                    Email = body.Email
                });

            return result;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}